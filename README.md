# Cross-compilation toolchain for TVSC projects

This repository contains a cross-compilation toolchain geared towards projects developed by TVSC. This toolchain comes directly from ARM.

## Details

This is a gcc 10.2 toolchain running on x86_64 Linux targeting ARM processors running linux. This toolchain comes directly from ARM: https://developer.arm.com/downloads/-/gnu-a

It includes one modification from that downloaded version. The ld script for libc.so included absolute paths for the dependent libraries. These absolute paths did not work in our sysroot'd configuration. I edited that file to add a '=' indicating that it should base the paths off of sysroot. I left a copy of the original file in the same directory under the name libc.so.orig.
